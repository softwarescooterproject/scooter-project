# Scooter Project

This project involves the development of a software system for hiring electric scooters in Leeds City
Centre.

This repository is for hosting the wiki for all of the sub-projects. Due to the structure of our project, we require repositories for each individual stack (mobile app, web app and API) and so rather than having individual wiki's for each sub-project, we have collated it all here instead. The wiki can be found here: https://gitlab.com/softwarescooterproject/scooter-project/-/wikis/Home

The Jira board for this project can be found at https://softwarescooterproject.atlassian.net/jira/software/projects/SEP/boards/1
